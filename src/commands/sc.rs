use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::Context,
};

use regex::Regex;

async fn change_name(ctx: &Context, msg: &Message, sc: u16, msc: u16) -> Result<(), String> {
    let guild = msg.guild(&ctx).unwrap();
    let member = &guild.member(&ctx, msg.author.id).await.unwrap();
    let name = member.display_name().into_owned();
    //puts all the matches into a vec
    let matches: Vec<_> = Regex::new(r"(?i)^(.*?)(\(SC\s?#\s?[-?\d\+\s?]*\)|$)$")
        .unwrap()
        .captures(&name)
        .unwrap()
        .iter()
        .map(|m| m.expect("").as_str())
        .collect();
    let tag = match msc {
        0 => format!("(SC#{})", sc),
        _ => format!("(SC#{}+{})", sc, msc),
    };
    let new_name = matches[1].to_owned() + &tag;
    match member.edit(&ctx, |m| m.nickname(&new_name)).await {
        Ok(_) => (),
        Err(e) => {
            return Err(e.to_string());
        }
    }
    let _ = msg
        .channel_id
        .say(&ctx, format!("Changed your name to {}", &new_name))
        .await;
    Ok(())
}

#[command]
#[min_args(1)]
#[max_args(2)]
#[sub_commands(clear)]
#[only_in("guilds")]
async fn sc(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut m_showcase: u16 = 0;
    let showcase = args.single::<u16>()?;
    if !args.is_empty() {
        m_showcase = args.single::<u16>()?;
    }
    if showcase > 112 || m_showcase > 48 {
        msg.channel_id
            .say(&ctx, String::from("SC Count too high!"))
            .await?;
    } else {
        match change_name(&ctx, msg, showcase, m_showcase).await {
            Ok(_) => (),
            Err(e) => {
                let _ = msg.channel_id.say(&ctx, e).await;
            }
        }
    }
    Ok(())
}

#[command]
#[only_in("guilds")]
async fn clear(ctx: &Context, msg: &Message) -> CommandResult {
    let re = Regex::new(r"\(SC#[^)]*\)$").unwrap();
    let guild = msg.guild(&ctx).unwrap();
    let member = &guild.member(&ctx, msg.author.id).await.unwrap();
    let name = member.display_name().into_owned();
    let stripped_name = re.replace(&name, "");
    member.edit(&ctx, |m| m.nickname(&stripped_name)).await?;
    msg.channel_id
        .say(&ctx, format!("Changed your name to {}", &stripped_name))
        .await?;
    Ok(())
}

use serenity::{
    builder::CreateMessage,
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::Context,
};

//use serenity_utils::{menu::Menu, menu::MenuOptions};

use chrono::offset::Utc;
use clap::{App, AppSettings, Arg, ColorChoice};

use crate::DBPool;

use crate::utils::parsers::parse_member;

enum ActionType {
    Added,
    Remvoved
}

#[command]
#[required_permissions(MANAGE_GUILD)]
#[sub_commands(list)]
#[only_in("guilds")]
/// Adds or removes points from a person
async fn points(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let cfg = data.get::<crate::ConfigContainer>().unwrap().lock().await;
    let usage = format!(
        "Usage: `{}points [FLAGS] <user> <amount>`",
        &cfg.prefix
    );
    let matches_res = App::new("Community event leaderboards")
        .args(&[
            Arg::new("remove")
                .short('r')
                .long("remove")
                .help("Removes the specified amount of points"),
            Arg::new("total")
                .short('t')
                .long("total")
                .help("Removes the specified amounf of points from the total, NOT the normal count."),
            Arg::new("user")
                .index(1)
                .required(true)
                .help("The user whose points you want to increase/decrease"),
            Arg::new("points")
                .index(2)
                .required(true)
                .help("The amount of points you want to add/remove"),
        ])
        .override_usage(usage.as_str())
        .bin_name(format!("{}points", cfg.prefix))
        .color(ColorChoice::Never)
        .setting(AppSettings::DisableVersionFlag)
        .try_get_matches_from(msg.content.split(" "));

    match matches_res {
        Ok(m) => {
            let rdata = ctx.data.read().await;
            let pool = rdata.get::<DBPool>().unwrap();

            let member_mention = m.value_of("user").unwrap();
            let member_res = parse_member(&ctx, &msg, member_mention.to_string()).await;
            if let Err(_) = member_res {
                let _ = msg.channel_id.say(&ctx, "Couldn't find that user").await;
                return Ok(());
            }
            let member = member_res.unwrap();
            let action: ActionType;

            let points_amount = m.value_of("points").unwrap().parse::<i32>().unwrap();
            //remove from both
            if m.is_present("total") && m.is_present("remove") {
                let _ = sqlx::query!(
                    "UPDATE community_events SET total_points = GREATEST(total_points - $2, 0), points = GREATEST(points - $2, 0) WHERE user_id = $1",
                    member.user.id.0 as i64,
                    points_amount,
                )
                .execute(pool)
                .await;
                action = ActionType::Remvoved;
            // remove total only
            } else if m.is_present("total") {
                let _ = sqlx::query!(
                    "UPDATE community_events SET total_points = GREATEST(total_points - $2, 0) WHERE user_id = $1",
                    member.user.id.0 as i64,
                    points_amount,
                )
                .execute(pool)
                .await;
                action = ActionType::Remvoved;
            //remove from available only
            }else if m.is_present("remove") {
                let _ = sqlx::query!(
                    "UPDATE community_events SET points = GREATEST(points - $2, 0) WHERE user_id = $1",
                    member.user.id.0 as i64,
                    points_amount,
                )
                .execute(pool)
                .await;
                action = ActionType::Remvoved;
            } else {
            // add to both
                let _ = sqlx::query!(
                    "INSERT INTO community_events (user_id, points, total_points)
                     VALUES ($1, $2, $2)
                     ON CONFLICT (user_id) DO
                     UPDATE SET points = community_events.points + $2, total_points = community_events.total_points + $2",
                    member.user.id.0 as i64,
                    points_amount,
                )
                .execute(pool)
                .await;
                action = ActionType::Added;
            }
            let _ = match action {
                ActionType::Remvoved => msg.channel_id.say(&ctx, format!("Successfully removed {} points from {}", points_amount, member.user.name)).await,
                ActionType::Added => msg.channel_id.say(&ctx, format!("Successfully added {} points to {}", points_amount, member.user.name)).await,
            };
        }
        Err(e) => {
            let _ = msg
                .channel_id
                .say(&ctx, format!("```{:?}```", e.info.get(0).unwrap()))
                .await;
        }
    }

    Ok(())
}

#[command]
#[only_in("guilds")]
/// Lists the users, sorting by points
async fn list(ctx: &Context, msg: &Message, _: Args) -> CommandResult {
    let rdata = ctx.data.read().await;
    let pool = rdata.get::<DBPool>().unwrap();

    let top_users =
        sqlx::query!("SELECT user_id, points, total_points FROM community_events ORDER BY total_points DESC")
            .fetch_all(pool)
            .await
            .unwrap();

    let mut pages: Vec<CreateMessage> = Vec::new();

    let chunks = top_users.chunks(10);
    for (_, users) in chunks.enumerate() {
        let mut leaderboards = String::new();
        for user in users {
            leaderboards.push_str(&format!("<@{}>: {} ({} available)\n", user.user_id, user.total_points, user.points));
        }
        let mut message = CreateMessage::default();
        message.embed(|e| {
            e.author(|a| {
                a.name("Community event leaderboards");
                a
            });
            e.description(leaderboards);
            e.color(0x9c31a0);
            e.timestamp(Utc::now());
            e
        });
        pages.push(message);
    }
    //let menu = Menu::new(ctx, msg, &pages, MenuOptions::default());

    //let _ = menu.run().await;

    //TODO: fix
    let _ = msg.channel_id.say(ctx, "not implemented").await;

    Ok(())
}

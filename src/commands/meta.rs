use serenity::{
    client::bridge::gateway::ShardId,
    framework::standard::{macros::command, CommandResult},
    model::channel::Message,
    prelude::Context,
};

use crate::ShardManagerContainer;

#[command]
#[aliases(latency)]
/// Shows the bots latency to this servers shard
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;

    // gets the shard manager
    let shard_manager = match data.get::<ShardManagerContainer>() {
        Some(v) => v,
        None => {
            let _ = msg
                .reply(ctx, "There was a problem getting the shard manager")
                .await;

            return Ok(());
        }
    };

    // locks the shard manager to prevent deadlocks
    let manager = shard_manager.lock().await;
    let runners = manager.runners.lock().await;

    // gets the Shard runner responsible for the shard this command was sent on
    let runner = match runners.get(&ShardId(ctx.shard_id)) {
        Some(runner) => runner,
        None => {
            let _ = msg.reply(ctx, "No shard found").await;

            return Ok(());
        }
    };

    // gets the actual latency, and cuts it down to 2 decimal places
    let latency;
    match runner.latency {
        Some(lat_raw) => latency = format!("{:.2}", lat_raw.as_secs_f64() * 1000.0),
        _ => latency = String::new(),
    }

    let _ = msg
        .channel_id
        .say(ctx, &format!("The shard latency is {}ms", latency))
        .await;
    Ok(())
}

#[command]
/// Sends a link tho the bots source code
async fn source(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id
        .say(ctx, "<https://gitlab.com/Felix422/CookieClubRS/>")
        .await?;
    Ok(())
}

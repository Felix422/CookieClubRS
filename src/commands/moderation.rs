use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::{channel::Message, id::MessageId},
    prelude::Context,
};

use crate::{utils::parsers::parse_member, DBPool};

#[command]
#[required_permissions(BAN_MEMBERS)]
#[min_args(1)]
#[only_in("guilds")]
/// Bans the specified user
async fn ban(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let pool = {
        let lock = ctx.data.read().await;
        lock.get::<DBPool>().unwrap().clone()
    };
    let member_name: String = args.single_quoted()?;
    let reason = args.remains();
    let date = sqlx::types::time::OffsetDateTime::now_utc().date();
    let member = match parse_member(&ctx, &msg, member_name.clone()).await {
        Ok(m) => m,
        Err(e) => {
            // If the member isnt in the guild anymore, we just ban them anyway
            if let Ok(id) = member_name.parse::<u64>() {
                msg.guild_id.unwrap().ban(&ctx, id, 7).await?;
                let _ = sqlx::query!("INSERT INTO case_info (infraction_type, user_id, moderator_id, date, reason) VALUES ('ban', $1, $2, $3, $4)", id as i64, msg.author.id.0 as i64, date, reason).execute(&pool).await;
                msg.channel_id.say(&ctx, "Banned user").await?;
            } else {
                msg.channel_id.say(&ctx, e).await?;
            }
            return Ok(());
        }
    };

    match member.permissions(&ctx) {
        Ok(perms) => {
            if !perms.administrator() {
                let case_id = sqlx::query!(
                    "INSERT INTO case_info (infraction_type, user_id, moderator_id, username, date, reason) VALUES ('ban', $1, $2, $3, $4, $5) RETURNING case_id",
                    member.user.id.0 as i64,
                    msg.author.id.0 as i64,
                    format!("{}#{}", member.user.name, member.user.discriminator),
                    date,
                    reason
                ).fetch_one(&pool).await.unwrap().case_id;
                let _ = member.user.dm(&ctx, |m| {
                    m.content(format!("
                        You've been banned from PIXELCUBE STUDIOS!\n\n \
                        **Your Case ID**: {}\n \
                        **Ban reason**: {}\n \
                        **Link to appeal form**: https://forms.gle/xVrpu4jdxCKb5NeU6\n\n\
                        You must **wait 1 week** before appealing or you will be automatically denied.
                        ",
                        case_id,
                        reason.unwrap_or("None")          
                    ))
                }).await;
                match reason {
                    Some(s) => {
                        let _ = member.ban_with_reason(&ctx, 0, s).await;
                        let _ = msg.channel_id.say(&ctx, format!("Banned user with reason {} and case id {}", s, case_id)).await;
                    }
                    None => {
                        let _ = member.ban(&ctx, 0).await;
                        let _ = msg.channel_id.say(&ctx, format!("Banned user with case id {}", case_id)).await;
                    }
                };
            } else {
                msg.channel_id
                    .say(&ctx, "That user is an administrator, i can't do that")
                    .await?;
            }
        }
        Err(_) => {
            msg.channel_id
                .say(
                    &ctx,
                    "Internal error while fetching user permissions, try again",
                )
                .await?;
            return Ok(());
        }
    }

    Ok(())
}

#[command]
#[min_args(1)]
#[required_permissions(BAN_MEMBERS)]
#[only_in("guilds")]
async fn baninfo(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let case_id = args.single::<i32>().unwrap();
    let pool = {
        let lock = ctx.data.read().await;
        lock.get::<DBPool>().unwrap().clone()
    };
    let info = sqlx::query!("SELECT case_id, user_id, date, moderator_id, username, reason FROM case_info WHERE case_id = $1 AND infraction_type = 'ban'", case_id)
        .fetch_one(&pool)
        .await.unwrap();
    
    let _ = msg.channel_id.send_message(&ctx, |m| {
        m.embed(|e | {
            e.title(format!("Case {} | {}", info.case_id, info.username));
            e.color(0xf04a47);
            e.fields(vec![
                ("User", info.username.clone(), true),
                ("Moderator", format!("<@{}>", info.moderator_id), true)
            ]);
            if let Some(x) = &info.reason {
                e.field("Reason", x, true);
            } 
            e.footer(|f| {
                f.text(format!("ID: {} • {}", info.user_id, info.date.format("%d/%m/%y")));
                f
            });
            e
        });
        m
    }).await;
    Ok(())
}

#[command]
#[required_permissions(KICK_MEMBERS)]
#[min_args(1)]
#[only_in("guilds")]
/// Kicks the specified user
async fn kick(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let member_arg = args.single_quoted::<String>()?;
    let member = parse_member(&ctx, &msg, member_arg).await;

    let reason = args.remains();

    let pool = {
        let lock = ctx.data.read().await;
        lock.get::<DBPool>().unwrap().clone()
    };

    match member {
        Ok(m) => {
            sqlx::query!(
                "INSERT INTO case_info (infraction_type, user_id, moderator_id, username, date, reason) VALUES ('kick', $1, $2, $3, $4, $5)",
                m.user.id.0 as i64,
                msg.author.id.0 as i64,
                format!("{}#{}", m.user.name, m.user.discriminator),
                sqlx::types::time::OffsetDateTime::now_utc().date(),
                reason
            ).execute(&pool).await?;
            if let Some(r) = reason {
                m.kick_with_reason(ctx, r).await?;
            } else {
                m.kick(ctx).await?;
            }
            let name = m.display_name();
            msg.channel_id
                .say(&ctx, format!("Successfully kicked {}", name))
                .await?;
        }
        Err(why) => {
            msg.channel_id.say(&ctx, why.to_string()).await?;
        }
    };

    Ok(())
}

#[command]
#[required_permissions(MANAGE_MESSAGES)]
#[num_args(1)]
#[only_in("guilds")]
#[aliases(clear)]
/// Deletes the specified amount of messages
async fn purge(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let amount = args.single::<u64>();
    match amount {
        Err(_) => {
            msg.channel_id
                .say(&ctx, "The value provided was not a valid number")
                .await?;
        }
        Ok(n) => {
            let channel = &msg.channel(&ctx).await.unwrap().guild().unwrap();

            let messages = &channel
                .messages(&ctx, |r| r.before(&msg.id).limit(n))
                .await?;
            let messages_ids = messages.iter().map(|m| m.id).collect::<Vec<MessageId>>();

            channel.delete_messages(&ctx, messages_ids).await?;

            msg.channel_id
                .say(&ctx, format!("Successfully deleted `{}` message", n))
                .await?;
        }
    }
    Ok(())
}

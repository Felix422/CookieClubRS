use chrono::offset::Utc;
use serde::{Deserialize, Serialize};
use serde_aux::prelude::deserialize_number_from_string;
use serenity::{
    async_trait,
    builder::CreateEmbed,
    client::bridge::gateway::ShardManager,
    client::ClientBuilder,
    framework::standard::{macros::group, StandardFramework},
    http::Http,
    model::{
        channel::Message,
        channel::Reaction,
        channel::ReactionType,
        event::MessageUpdateEvent,
        gateway::Ready,
        guild::{Member, Role},
        id::{ChannelId, GuildId, MessageId, RoleId, UserId},
        user::User,
    },
    prelude::*,
};
use sqlx::{
    postgres::{PgPool, PgPoolOptions},
    types::time::OffsetDateTime,
};
use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::prelude::*,
    net::SocketAddrV4,
    sync::Arc,
    time::Duration,
};
use tokio::sync::Mutex;
use toml;
use warp::Filter;

use lettre::{
    transport::smtp::authentication::Credentials, AsyncSmtpTransport, AsyncTransport,
    Message as EmailMessage, Tokio1Executor,
};

use tracing::{info, Level};

use tracing_log::LogTracer;
use tracing_subscriber::FmtSubscriber;

mod events;
use events::logging;

mod commands;
pub mod utils;
use commands::comm_events::*;
use commands::meta::*;
use commands::moderation::*;
use commands::sc::*;

const PIXELCUBE_GUILD_ID: u64 = 232297309847814144;
const PIXELCUBE_BAN_APPEAL_CHANNEL: u64 = 778682781781459014;
const STAFF_AMOUNT: usize = 6;

#[group("Info")]
#[commands(ping, source)]
#[description("Basic Commands")]
struct Meta;

#[group("Moderation")]
#[commands(ban, baninfo, kick, purge)]
#[description("Commands related to Moderation")]
struct Moderation;

#[group("Showcase tags")]
#[commands(sc)]
#[description("Commands for changing your Showcase tag")]
struct SC;

#[group("Community events")]
#[commands(points)]
#[description("Commands related to the Community events")]
struct CommunityEvents;

struct ShardManagerContainer;
struct DBPool;
struct ConfigContainer;

impl TypeMapKey for DBPool {
    type Value = PgPool;
}

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

impl TypeMapKey for ConfigContainer {
    type Value = Arc<Mutex<Config>>;
}

struct Handler;

pub enum LogType {
    MemberLog,
    AuditLog,
}

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct Config {
    discord_token: String,
    db_bind: String,
    prefix: String,
    enable_tracing: bool,
    trace_level: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Appeal {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    case_number: u64,
    email: String,
    appeal_reason: String,
    additional_info: String,
}

async fn add_appeal(
    appeal: Appeal,
    ctx: Arc<Context>,
) -> Result<impl warp::Reply, warp::Rejection> {
    dbg!(&appeal);
    let data = ctx.data.read().await;
    let pool = data.get::<DBPool>().expect("Expected DB pool in Typemap");

    let ban_info = sqlx::query!(
        "SELECT user_id, date, moderator_id, username, reason FROM case_info WHERE case_id = $1 AND infraction_type = 'ban' ORDER BY date DESC",
        appeal.case_number as i64
    )
    .fetch_one(pool)
    .await
    .unwrap();

    let guild_bans = ctx
        .http
        .get_bans(PIXELCUBE_GUILD_ID)
        .await
        .expect("Error fetching guild bans");

    if !(guild_bans
        .iter()
        .any(|x| x.user.id.0 as i64 == ban_info.user_id))
    {
        //User isnt Banned, so chuck it
        return Ok(warp::http::StatusCode::OK);
    }

    let eligible = {
        let appeal_count: i64 = sqlx::query!(
            r#"SELECT COUNT(*) "count!" FROM appeals WHERE user_id = $1"#,
            ban_info.user_id
        )
        .fetch_one(pool)
        .await
        .unwrap()
        .count;
        match appeal_count {
            // 1 week if this is the first appeal
            0 => {
                ban_info.date + Duration::from_secs(60 * 60 * 24 * 7)
                    < OffsetDateTime::now_utc().date()
            }
            // if their first appeal was accepted, next appeal gets declined
            // if it wasnt, the cooldown is 6 months
            1 => {
                let result: bool = sqlx::query!(
                    r#"SELECT result "result!" FROM appeals WHERE user_id = $1 ORDER BY appeal_number DESC"#,
                    ban_info.user_id
                ).fetch_one(pool).await.unwrap().result;
                if result {
                    false
                } else {
                    ban_info.date + Duration::from_secs(60 * 60 * 24 * 30 * 6)
                        < OffsetDateTime::now_utc().date()
                }
            }
            // if this is their 3rd appeal or more for whatever reason, it gets declined
            _ => false,
        }
    };

    if eligible {
        // Eligible for appeal

        //Send message to appeal channel
        let message = ChannelId(PIXELCUBE_BAN_APPEAL_CHANNEL)
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.color(0x202369);
                    e.timestamp(Utc::now());
                    e.author(|a| {
                        a.name(format!(
                            "CASE {} | {} | {}",
                            appeal.case_number, ban_info.user_id, format!("<@{}>", ban_info.moderator_id)
                        ));
                        a
                    });
                    e.fields(vec![
                        ("\u{200b}", "**INFORMATION**", false),
                        (
                            "**DATE OF BAN**",
                            &ban_info.date.format("%d/%m %Y"),
                            true,
                        ),
                        ("**Moderator**", &format!("<@{}>", ban_info.moderator_id), true),
                        (
                            "**BAN REASON**",
                            &ban_info.reason.clone().unwrap_or(String::from("None")),
                            false,
                        ),
                        ("\u{200b}", "**APPEAL**", false),
                        ("**» Appeal reason**", &appeal.appeal_reason, false),
                        (
                            "**» Additional information**",
                            &appeal.additional_info,
                            false,
                        ),
                    ]);
                    e
                });
                m
            })
            .await
            .expect("error sending message");
        // Add yes and no reactions
        let _ = message
            .react(&ctx.http, ReactionType::Unicode("✅".into()))
            .await;
        let _ = message
            .react(&ctx.http, ReactionType::Unicode("❌".into()))
            .await;

        // insert appeal into db
        let _ = sqlx::query!(
            "INSERT INTO appeals (message_id, user_id, email) VALUES ($1, $2, $3);",
            message.id.0 as i64,
            ban_info.user_id as i64,
            appeal.email
        )
        .fetch_one(pool)
        .await;
    }
    // else its ineligible

    Ok(warp::http::StatusCode::OK)
}

async fn send_unban_email(address: String, unbanned: bool) {
    let email = EmailMessage::builder()
        .from(env!("EMAIL_ADDRESS").parse().unwrap())
        .to(address.parse().unwrap())
        .subject("Pixelcube ban appeal")
        .body(
            if unbanned {
                String::from("Thank you for your appeal, it has now been processed. \
                Your account will be unbanned from PIXELCUBE STUDIOS discord server. \
                Make sure you take the time to read through the rules in the welcome channel, and enjoy your stay. \
                Should another ban occur, it will be final and permanent so make sure to read the rules carefully.\n\n

                - Discord staff")
            } else {
                String::from("Thank you for your appeal, it has now been processed. \
                Your account will remain banned from PIXELCUBE STUDIOS discord server. \
                If this was your first appeal you are eligible to appeal again in 6 months, \
                if it was your second one your ban is now permanent.\n\n

                - Discord staff")
            }
        )
        .unwrap();

    let creds = Credentials::new(
        env!("EMAIL_ADDRESS").to_string(),
        env!("EMAIL_PASSWORD").to_string(),
    );

    // Open a remote connection to gmail using STARTTLS
    let mailer = AsyncSmtpTransport::<Tokio1Executor>::starttls_relay("smtp.office365.com")
        .unwrap()
        .credentials(creds)
        .build();

    // Send the email
    match mailer.send(email).await {
        Ok(_) => (),
        Err(e) => println!("Could not send email: {:?}", e),
    }
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
    async fn cache_ready(&self, ctx: Context, _guilds: Vec<GuildId>) {
        let ctx = Arc::new(ctx);
        let ctx_clone = Arc::clone(&ctx);
        let ctx_filter = warp::any().map(move || ctx_clone.clone());

        tokio::spawn(async move {
            let routes = warp::post()
                .and(warp::path("appeal"))
                .and(warp::path::end())
                .and(warp::body::json())
                .and(ctx_filter)
                .and_then(add_appeal);

            // just to automate the ip change for my pi
            let adress: SocketAddrV4 = if cfg!(target_arch = "arm") {
                "192.168.0.104:4444".parse().unwrap()
            } else {
                "79.193.223.139:4444".parse().unwrap()
            };

            warp::serve(routes).run(adress).await;
        });
    }
    #[allow(unused_variables)]
    async fn reaction_add(&self, ctx: Context, reaction: Reaction) {
        let data = ctx.data.read().await;
        let pool = data.get::<DBPool>().expect("Expected DB pool in Typemap");

        let appeal_info_res = sqlx::query!(
            "SELECT appeal_number, user_id, email from appeals WHERE message_id = $1;",
            reaction.message_id.0 as i64
        )
        .fetch_one(pool)
        .await;

        let appeal_info = match appeal_info_res {
            Ok(a) => a,
            _ => return,
        };
        // Appeal with that mesage id found
        let mut msg = reaction.message(&ctx).await.unwrap();
        // remove reaction if not valid vote
        if !(["✅", "❌"]
            .iter()
            .any(|unicode| reaction.emoji.unicode_eq(unicode)))
        {
            let _ = msg.delete_reaction_emoji(&ctx, reaction.emoji).await;
            return;
        }
        let reactions = &msg.reactions;

        let mut voters: HashSet<UserId> = HashSet::new();

        for reaction in reactions {
            let reaction_users = msg
                .reaction_users(&ctx, reaction.reaction_type.clone(), None, None)
                .await
                .unwrap();
            for user in reaction_users {
                voters.insert(user.id);
            }
        }
        // gotta include the bot
        if voters.len() >= STAFF_AMOUNT + 1 {
            // vote concluded

            // get all reactions
            let reactions = ctx
                .http
                .get_message(PIXELCUBE_BAN_APPEAL_CHANNEL, reaction.message_id.0)
                .await
                .unwrap()
                .reactions;
            let mut emojis: HashMap<String, u64> = HashMap::new();
            // map reaction emojis to the number of reactions
            for reaction in reactions.iter() {
                emojis.insert(reaction.reaction_type.to_string(), reaction.count - 1);
            }
            let mut embed = CreateEmbed::from(msg.embeds.get(0).unwrap().clone());
            let unban = emojis.get("✅") > emojis.get("❌");
            if unban {
                // Vote for unban
                let guild_id = GuildId(PIXELCUBE_GUILD_ID);
                let _ = guild_id
                    .unban(&ctx, UserId(appeal_info.user_id as u64))
                    .await;
                send_unban_email(appeal_info.email, true).await;
                embed.field("**» RESULT**", "Unbanned", false);
                let _ = msg
                    .edit(&ctx.http, |m| {
                        m.embed(|e| {
                            *e = embed;
                            e
                        });
                        m
                    })
                    .await;
            } else {
                // Vote No
                send_unban_email(appeal_info.email, false).await;
                embed.field("**» RESULT**", "Still banned", false);
                let _ = msg
                    .edit(&ctx.http, |m| {
                        m.embed(|e| {
                            *e = embed;
                            e
                        });
                        m
                    })
                    .await;
            }
            let _ = sqlx::query!(
                "UPDATE appeals SET result = $1 WHERE appeal_number = $2",
                unban,
                appeal_info.appeal_number
            )
            .execute(pool)
            .await;
            let _ = msg.delete_reactions(&ctx.http).await;
        }
    }
    async fn message_delete(
        &self,
        ctx: Context,
        channel_id: ChannelId,
        deleted_message_id: MessageId,
        guild_id: Option<GuildId>,
    ) {
        logging::message_delete(ctx, channel_id, deleted_message_id, guild_id).await
    }
    async fn message_update(
        &self,
        ctx: Context,
        old_if_available: Option<Message>,
        new: Option<Message>,
        event: MessageUpdateEvent,
    ) {
        logging::message_update(ctx, old_if_available, new, event).await
    }
    async fn message_delete_bulk(
        &self,
        ctx: Context,
        channel_id: ChannelId,
        deleted_messages: Vec<MessageId>,
        guild_id: Option<GuildId>,
    ) {
        logging::message_delete_bulk(ctx, channel_id, deleted_messages, guild_id).await;
    }
    async fn guild_member_update(
        &self,
        ctx: Context,
        old_if_available: Option<Member>,
        new: Member,
    ) {
        logging::guild_member_update(ctx, old_if_available, new).await;
    }
    async fn guild_role_create(&self, ctx: Context, new: Role) {
        logging::guild_role_create(ctx, new.guild_id, new).await;
    }
    async fn guild_role_delete(
        &self,
        ctx: Context,
        guild_id: GuildId,
        removed_role_id: RoleId,
        role_if_available: Option<Role>,
    ) {
        logging::guild_role_delete(ctx, guild_id, removed_role_id, role_if_available).await;
    }
    async fn guild_role_update(
        &self,
        ctx: Context,
        old_if_available: Option<Role>,
        new: Role,
    ) {
        logging::guild_role_update(ctx, new.guild_id, old_if_available, new).await;
    }
    async fn guild_member_addition(&self, ctx: Context, new_member: Member) {
        logging::guild_member_addition(ctx, new_member.guild_id, new_member).await;
    }
    async fn guild_member_removal(
        &self,
        ctx: Context,
        guild_id: GuildId,
        user: User,
        member_data_if_available: Option<Member>,
    ) {
        logging::guild_member_removal(ctx, guild_id, user, member_data_if_available).await;
    }
}
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut file = File::open("config.toml")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let config: Config = toml::from_str(&contents).expect("Error parsing config file");

    if config.enable_tracing {
        LogTracer::init()?;
        let base_level = config.trace_level.as_str();
        let level = match base_level {
            "error" => Level::ERROR,
            "warn" => Level::WARN,
            "info" => Level::INFO,
            "debug" => Level::DEBUG,
            "trace" => Level::TRACE,
            _ => Level::TRACE,
        };

        info!("Tracer initialized.");

        let subscriber = FmtSubscriber::builder().with_max_level(level).finish();
        tracing::subscriber::set_global_default(subscriber)?;
    }

    let mut intents = GatewayIntents::all();
    intents.remove(GatewayIntents::DIRECT_MESSAGE_TYPING);
    intents.remove(GatewayIntents::GUILD_MESSAGE_TYPING);

    let http = Http::new(config.discord_token.as_str());

    let owners = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            if let Some(t) = info.team {
                for member in t.members.iter() {
                    owners.insert(member.user.id);
                }
            } else {
                owners.insert(info.owner.id);
            }
            owners
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    let prefix = config.prefix.as_str().clone();
    let framework = StandardFramework::new()
        .configure(|c| c.with_whitespace(false).prefix(prefix).owners(owners))
        .group(&META_GROUP)
        .group(&MODERATION_GROUP)
        .group(&SC_GROUP)
        .group(&COMMUNITYEVENTS_GROUP);

    let mut client = ClientBuilder::new(config.discord_token.as_str(), intents)
        .event_handler(Handler)
        .framework(framework)
        .await?;
    client.cache_and_http.cache.set_max_messages(200);
    {
        let mut data = client.data.write().await;
        let pool = PgPoolOptions::new()
            .max_connections(5)
            .connect(config.db_bind.as_str())
            .await?;
        data.insert::<DBPool>(pool);
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
        data.insert::<ConfigContainer>(Arc::new(Mutex::new(config)));
    }

    if let Err(why) = client.start().await {
        eprintln!("An error occurred while running the client: {:?}", why);
    };
    Ok(())
}

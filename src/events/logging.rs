#![allow(dead_code)]
#![allow(unused_variables)]
use chrono::offset::Utc;
use serenity::{
    model::{
        channel::Message,
        event::MessageUpdateEvent,
        guild::{Action, MemberAction, Member, Role},
        id::{ChannelId, GuildId, MessageId, RoleId},
        user::User,
    },
    prelude::Context,
};
use std::collections::HashSet;
use std::iter::FromIterator;

use crate::DBPool;
use crate::LogType;

async fn get_log_channel(ctx: &Context, guild_id: GuildId, log_type: LogType) -> Option<ChannelId> {
    let data = ctx.data.read().await;
    let pool = data.get::<DBPool>().unwrap();

    let id = match log_type {
        LogType::AuditLog => {
            match sqlx::query!(
                "SELECT action_log FROM log WHERE guild_id = $1",
                *guild_id.as_u64() as i64
            )
            .fetch_optional(pool)
            .await
            .unwrap()
            {
                Some(r) => r,
                None => return None,
            }
            .action_log
        }

        LogType::MemberLog => {
            match sqlx::query!(
                "SELECT member_log FROM log WHERE guild_id = $1",
                *guild_id.as_u64() as i64
            )
            .fetch_optional(pool)
            .await
            .unwrap()
            {
                Some(r) => r,
                None => return None,
            }
            .member_log
        }
    };

    match id {
        Some(x) => {
            let channel_id = ChannelId(x as u64);
            Some(channel_id)
        }
        None => None,
    }
}

pub async fn message_delete(
    ctx: Context,
    channel_id: ChannelId,
    deleted_message_id: MessageId,
    guild_id_opt: Option<GuildId>,
) {
    let guild_id = match guild_id_opt {
        Some(id) => id,
        None => {
            let channel = channel_id.to_channel(&ctx).await.unwrap();
            channel.guild().unwrap().guild_id
        }
    };
    // dont log management chat
    if [689976335036055575, 767928086963552287].contains(&channel_id.0) {
        return;
    }

    if let Some(channel) = get_log_channel(&ctx, guild_id, LogType::AuditLog).await {
        if let Some(msg) = ctx.cache.message(channel_id, &deleted_message_id) {
            if msg.author.id == ctx.cache.current_user_id() {
                return;
            }
            let _ = channel
                .send_message(&ctx, |m| {
                    m.embed(|e| {
                        e.timestamp(Utc::now());
                        e.color(0xFF0000);
                        e.description(format!(
                            "**message sent by <@{}> deleted in <#{}>** \n{}",
                            msg.author.id, channel_id, msg.content
                        ));
                        e.author(|a| {
                            a.name(&msg.author.name);
                            a.icon_url(&msg.author.face());
                            a
                        });
                        e.footer(|f| {
                            f.text(format!(
                                "Author: {} | Message ID: {}",
                                msg.author.id, deleted_message_id
                            ));
                            f
                        });
                        e
                    })
                })
                .await;
        }
    };
}

pub async fn message_update(
    ctx: Context,
    old: Option<Message>,
    new: Option<Message>,
    _event: MessageUpdateEvent,
) {
    if old.is_none() || new.is_none() {
        return;
    }
    let old_m = old.unwrap();
    let new_m = new.unwrap();
    if old_m.content == new_m.content || new_m.guild_id.is_none() || new_m.author.bot {
        return;
    }
    // dont log management chat
    if [689976335036055575, 767928086963552287].contains(&old_m.channel_id.0) {
        return;
    }
    if let Some(log_channel) =
        get_log_channel(&ctx, new_m.guild_id.unwrap(), LogType::AuditLog).await
    {
        let _ = log_channel
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    let jump_url = format!(
                        "https://discord.com/channels/{}/{}/{}",
                        new_m.guild_id.unwrap(),
                        new_m.channel_id,
                        new_m.id
                    );
                    e.color(0x7289da);
                    e.timestamp(Utc::now());
                    e.description(format!(
                        "**Message edited in** <#{}> [Jump to message]({})",
                        new_m.channel_id, jump_url
                    ));
                    e.fields(vec![
                        ("Before", &old_m.content, false),
                        ("After", &new_m.content, true),
                    ]);
                    e.author(|a| {
                        a.name(format!(
                            "{}#{:04}",
                            new_m.author.name, new_m.author.discriminator
                        ));
                        a.icon_url(new_m.author.face());
                        a
                    });
                    e.footer(|f| {
                        f.text(format!("Author: {}", &new_m.author.id));
                        f
                    });
                    e
                });
                m
            })
            .await;
    }
}

pub async fn message_delete_bulk(
    ctx: Context,
    channel_id: ChannelId,
    deleted_messages: Vec<MessageId>,
    guild_id_opt: Option<GuildId>,
) {
    let guild_id = match guild_id_opt {
        Some(id) => id,
        None => {
            let channel = channel_id.to_channel(&ctx).await.unwrap();
            channel.guild().unwrap().guild_id
        }
    };
    let guild = match guild_id.to_partial_guild(&ctx).await {
        Ok(g) => g,
        Err(_) => return,
    };

    if let Some(log_channel) = get_log_channel(&ctx, guild_id, LogType::AuditLog).await {
        let _ = log_channel
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.color(0x7289da);
                    e.timestamp(Utc::now());
                    e.description(format!(
                        "**Bulk deleted {} message{} in <#{}>**",
                        deleted_messages.len(),
                        if deleted_messages.len() == 1 { "s" } else { "" },
                        channel_id
                    ));
                    e.author(|a| {
                        a.name(&guild.name);
                        let icon_url = guild.icon_url();
                        if let Some(url) = icon_url {
                            a.icon_url(&url);
                        };
                        a
                    });
                    e
                });
                m
            })
            .await;
    };
}

pub async fn guild_member_update(ctx: Context, old_if_available: Option<Member>, new: Member) {
    if old_if_available.is_none() {
        return;
    }
    let old = old_if_available.unwrap();

    let old_nick = old.nick.as_ref().map_or("None", |s| s.as_str());
    let new_nick = new.nick.as_ref().map_or("None", |s| s.as_str());
    let log_option = get_log_channel(&ctx, new.guild_id, LogType::AuditLog).await;
    if log_option.is_none() {
        return;
    }
    let log_channel = log_option.unwrap();

    if new_nick != old_nick {
        let _ = log_channel
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.color(0x7289da);
                    e.timestamp(Utc::now());
                    e.description(format!("<@{}> Nickname changed", &new.user.id));
                    e.fields(vec![("Before", old_nick, true), ("After", new_nick, false)]);
                    e.author(|a| {
                        a.name(&old.user.name);
                        a.icon_url(&old.user.face());
                        a
                    });
                    e.footer(|f| {
                        f.text(format!("ID: {}", &old.user.id));
                        f
                    });
                    e
                });
                m
            })
            .await;
    } else if new.roles != old.roles {
        let old_roles = HashSet::from_iter(old.roles.iter().cloned());
        let new_roles = HashSet::from_iter(new.roles.iter().cloned());
        let added_roles: HashSet<RoleId> = &new_roles - &old_roles;
        let removed_roles: HashSet<RoleId> = &old_roles - &new_roles;
        let (description, field): (String, (&str, String, bool)) = if !added_roles.is_empty() {
            let mut names: Vec<String> = vec![];
            for role_id in added_roles.iter() {
                if let Some(role) = role_id.to_role_cached(&ctx) {
                    names.push(role.name);
                }
            }
            (
                format!("Added roles to {}", new.user.name),
                ("Added roles:", format!("`{}`", names.join("\n")), false),
            )
        } else {
            let mut names: Vec<String> = vec![];
            for role_id in removed_roles.iter() {
                if let Some(role) = role_id.to_role_cached(&ctx) {
                    names.push(role.name);
                }
            }
            (
                format!("Removed roles from {}", new.user.name),
                ("Removed roles:", format!("`{}`", names.join("\n")), false),
            )
        };
        let _ = log_channel
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.color(0x000000);
                    e.timestamp(Utc::now());
                    e.description(description);
                    e.fields(vec![field]);
                    e.author(|a| {
                        a.name(new.display_name());
                        a.icon_url(new.user.face());
                        a
                    });
                    e.footer(|f| {
                        f.text(format!("ID: {}", new.user.id));
                        f
                    });
                    e
                });
                m
            })
            .await;
    }
}

pub async fn guild_role_create(ctx: Context, guild_id: GuildId, new_role: Role) {
    let log_option = get_log_channel(&ctx, guild_id, LogType::AuditLog).await;
    if log_option.is_none() {
        return;
    }
    let log_channel = log_option.unwrap();
    let audit_log_res = guild_id
        .audit_logs(&ctx, Some(30_u8), None, None, Some(1_u8))
        .await;
    if let Ok(audit_log) = audit_log_res {
        for entry in audit_log.entries {
            let _ = log_channel
                .send_message(&ctx, |m| {
                    m.embed(|e| {
                        e.description(format!(
                            "**New role created by <@{}>**\nName: `{}`",
                            entry.user_id, new_role.name
                        ));
                        e.color(0x2ecc71);
                        e.timestamp(Utc::now());
                        e.footer(|f| {
                            f.text(format!("ID: {}", new_role.id));
                            f
                        });
                        e
                    });
                    m
                })
                .await;
        }
    }
}

pub async fn guild_role_delete(
    ctx: Context,
    guild_id: GuildId,
    removed_role_id: RoleId,
    role_if_available: Option<Role>,
) {
    let role_name = if role_if_available.is_none() {
        String::from("Unknown")
    } else {
        role_if_available.unwrap().name
    };
    let log_option = get_log_channel(&ctx, guild_id, LogType::AuditLog).await;
    if log_option.is_none() {
        return;
    }
    let log_channel = log_option.unwrap();

    let audit_log_res = guild_id
        .audit_logs(&ctx, Some(30_u8), None, None, Some(1_u8))
        .await;
    if let Ok(audit_log) = audit_log_res {
        for entry in audit_log.entries {
            let _ = log_channel
                .send_message(&ctx, |m| {
                    m.embed(|e| {
                        e.description(format!(
                            "**Role deleted by <@{}>**\nName: `{}`",
                            entry.user_id, role_name
                        ));
                        e.color(0xFF0000);
                        e.timestamp(Utc::now());
                        e.footer(|f| {
                            f.text(format!("ID: {}", removed_role_id));
                            f
                        });
                        e
                    });
                    m
                })
                .await;
        }
    }
}

pub async fn guild_role_update(
    ctx: Context,
    guild_id: GuildId,
    old_if_available: Option<Role>,
    new: Role,
) {
    let log_option = get_log_channel(&ctx, guild_id, LogType::AuditLog).await;
    if log_option.is_none() {
        return;
    }
    if old_if_available.is_none() {
        return;
    }
    let old = old_if_available.unwrap();
    let log_channel = log_option.unwrap();

    if old.name == new.name && old.permissions == new.permissions {
        return;
    }

    let perm_add = new.permissions - old.permissions;
    let perm_rem = old.permissions - new.permissions;

    let _ = log_channel
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.description(format!("Updated role `{}`", new.name));
                e.color(0x7289da);
                e.timestamp(Utc::now());
                e.footer(|f| {
                    f.text(format!("ID: {}", new.id));
                    f
                });
                let mut fields: Vec<(&str, String, bool)> = vec![];
                if old.name != new.name {
                    fields.push((
                        "Changed Name:",
                        format!("Old name: {}\nNew name: {}", old.name, new.name),
                        false,
                    ));
                }
                if !perm_add.is_empty() {
                    fields.push((
                        "Added permissions: ",
                        format!("{:?}", perm_add).replace("_", " ").to_lowercase(),
                        false,
                    ));
                }
                if !perm_rem.is_empty() {
                    fields.push((
                        "Removed permissions",
                        format!("{:?}", perm_rem).replace("_", " ").to_lowercase(),
                        false,
                    ));
                }
                e.fields(fields);
                e
            });
            m
        })
        .await;
}

pub async fn guild_member_addition(ctx: Context, guild_id: GuildId, member: Member) {
    let log_option = get_log_channel(&ctx, guild_id, LogType::MemberLog).await;
    if log_option.is_none() {
        return;
    }

    let log_channel = log_option.unwrap();
    let _ = log_channel
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.description(format!("<@{}> {}", member.user.id, member.distinct()));
                e.field(
                    "Created at:",
                    format!("{} UTC", member.user.created_at().format("%d %b %Y | %R")),
                    false,
                );
                e.footer(|f| {
                    f.text(format!("ID: {}", member.user.id));
                    f
                });
                e.author(|a| {
                    a.name(format!("{} has joined the server!", member.user.name));
                    a.icon_url(member.user.face());
                    a
                });
                e.color(0x2ecc71);
                e.timestamp(Utc::now());
                e.thumbnail(member.user.face());
                e
            });
            m
        })
        .await;
}

pub async fn guild_member_removal(
    ctx: Context,
    guild_id: GuildId,
    user: User,
    member_if_available: Option<Member>,
) {
    let log_option = get_log_channel(&ctx, guild_id, LogType::MemberLog).await;
    if log_option.is_none() {
        return;
    }

    let log_channel = log_option.unwrap();
    tokio::time::sleep(std::time::Duration::from_secs(2)).await;
    let audit_log_res = guild_id
        .audit_logs(&ctx, None, None, None, Some(1_u8))
        .await;

    if let Ok(audit_log) = audit_log_res {
        for entry in audit_log.entries {
            let target_id = match &entry.target_id {
                Some(id) => id,
                None => return,
            };
            let reason = &entry.reason;
            let action = entry.action;
            let _ = log_channel
                .send_message(&ctx, |m| {
                    m.embed(|e| {
                        e.description(format!(
                            "<@{}> {}#{:04}",
                            user.id, user.name, user.discriminator
                        ));
                        e.footer(|f| {
                            f.text(format!("ID: {}", user.id));
                            f
                        });
                        if let Action::Member(member_action) = action {
                            if target_id == &user.id.0 {
                                match member_action {
                                    MemberAction::BanAdd => {
                                        if let Some(r) = reason {
                                            e.field("Reason:", &r, false);
                                        }
                                        e.author(|a| {
                                            a.name(format!(
                                                "{} got banned from the server",
                                                user.name
                                            ));
                                            a.icon_url(user.face());
                                            a
                                        });
                                    }
                                    MemberAction::Kick => {
                                        if let Some(r) = reason {
                                            e.field("Reason:", &r, false);
                                        }
                                        e.author(|a| {
                                            a.name(format!(
                                                "{} got kicked from the server",
                                                user.name
                                            ));
                                            a.icon_url(user.face());
                                            a
                                        });
                                    }
                                    _ => (),
                                }
                            }
                        }
                        if target_id != &user.id.0 {
                            e.author(|a| {
                                a.name(format!("{} left the server", user.name));
                                a.icon_url(user.face());
                                a
                            });
                        }
                        e.color(0xFF0000);
                        e.timestamp(Utc::now());
                        e.thumbnail(user.face());
                        e
                    });
                    m
                })
                .await;
        }
    }
}

use futures::{stream, StreamExt};
use regex::Regex;
use serenity::{
    model::{
        channel::{GuildChannel, Message},
        guild::Member,
        id::{ChannelId, UserId},
    },
    prelude::Context,
};

pub async fn parse_member(
    ctx: &Context,
    msg: &Message,
    member_name: String,
) -> Result<Member, String> {
    let mut members = Vec::new();
    if let Ok(id) = member_name.parse::<u64>() {
        // if the input is an id
        let member = &msg.guild_id.unwrap().member(ctx, id).await;
        match member {
            Ok(m) => Ok(m.to_owned()),
            Err(why) => Err(why.to_string()),
        }
    } else if member_name.starts_with("<@") && member_name.ends_with('>') {
        // if the input is a mention
        let re = Regex::new("[<@!>]").unwrap();
        let member_id = re.replace_all(&member_name, "").into_owned();
        let member = &msg
            .guild_id
            .unwrap()
            .member(ctx, UserId(member_id.parse::<u64>().unwrap()))
            .await;

        match member {
            Ok(m) => Ok(m.to_owned()),
            Err(why) => Err(why.to_string()),
        }
    } else {
        // if the input is just a name
        let guild = &msg.guild(ctx).unwrap();
        let member_name = member_name.split('#').next().unwrap();

        for m in guild.members.values() {
            if m.display_name() == std::borrow::Cow::Borrowed(member_name)
                || m.user.name == member_name
            {
                members.push(m);
            }
        }

        if members.is_empty() {
            let similar_members = &guild.members_containing(&member_name, false, false).await;

            let mut members_string = stream::iter(similar_members.iter())
                .map(|m| async move {
                    let member = &m.0.user;
                    format!("`{}`|", member.name)
                })
                .fold(String::new(), |mut acc, c| async move {
                    acc.push_str(&c.await);
                    acc
                })
                .await;

            let message = {
                if members_string == "" {
                    format!("No member named '{}' was found.", member_name)
                } else {
                    members_string.pop();
                    format!(
                        "No member named '{}' was found.\nDid you mean: {}",
                        member_name, members_string
                    )
                }
            };
            Err(message)
        } else if members.len() == 1 {
            Ok(members[0].to_owned())
        } else {
            let mut members_string = stream::iter(members.iter())
                .map(|m| async move {
                    let member = &m.user;
                    format!("`{}#{}`|", member.name, member.discriminator)
                })
                .fold(String::new(), |mut acc, c| async move {
                    acc.push_str(&c.await);
                    acc
                })
                .await;

            members_string.pop();

            let message = format!(
                "Multiple members with the same name where found: '{}'",
                &members_string
            );
            Err(message)
        }
    }
}

pub async fn parse_channel(
    ctx: &Context,
    msg: &Message,
    channel_raw: String,
) -> Result<GuildChannel, String> {
    if let Ok(c) = channel_raw.parse::<u64>() {
        // if the input is an id
        let channel = ChannelId(c).to_channel(ctx).await;
        match channel {
            Ok(c) => Ok(c.guild().unwrap()),
            Err(why) => Err(why.to_string()),
        }
    } else if channel_raw.starts_with("<#") && channel_raw.ends_with('>') {
        // if the input is a mention
        let re = Regex::new("[<#!>]").unwrap();
        let channel_id = re.replace_all(&channel_raw, "").into_owned();
        let channel = ChannelId(channel_id.parse::<u64>().unwrap())
            .to_channel(ctx)
            .await;
        match channel {
            Ok(c) => Ok(c.guild().unwrap()),
            Err(why) => Err(why.to_string()),
        }
    } else {
        // if the input is just a name
        let guild = &msg.guild(ctx).unwrap();
        let channel_id = guild.channel_id_from_name(ctx, &channel_raw);
        match channel_id {
            // return the channel if found, a simple error message if not
            Some(c) => Ok(c.to_channel(ctx).await.unwrap().guild().unwrap()),
            None => Err(String::from("Couldn't find a channel with that name or id")),
        }
    }
}

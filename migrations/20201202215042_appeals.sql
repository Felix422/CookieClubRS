-- Add migration script here
CREATE SEQUENCE appeal_number_seq START 1;
CREATE TABLE appeals (
    appeal_number integer DEFAULT nextval('appeal_number_seq') NOT NULL,
    message_id bigint NOT NULL,
    user_id bigint NOT NULL,
    email text NOT NULL,
    result boolean
);
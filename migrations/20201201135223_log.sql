-- Add migration script here
CREATE TABLE public.log (
    member_log bigint,
    action_log bigint,
    ban_logs bigint,
    guild_id bigint NOT NULL,
    CONSTRAINT log_pkey PRIMARY KEY (guild_id)
);
-- Add migration script here
CREATE TYPE infraction_type AS ENUM ('ban', 'unban', 'warn', 'mute', 'kick');
CREATE TABLE case_info (
    case_id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    infraction_type infraction_type NOT NULL,
    user_id BIGINT NOT NULL,
    date DATE NOT NULL,   
    end_date DATE, 
    moderator_id bigint NOT NULL,
    username varchar(37) NOT NULL DEFAULT 'Unknown',
    reason varchar(512)
);
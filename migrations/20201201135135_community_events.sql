-- Add migration script here
CREATE TABLE public.community_events (
    user_id bigint PRIMARY KEY NOT NULL,
    points integer NOT NULL,
    total_points integer NOT NULL
);